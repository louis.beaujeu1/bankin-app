package com.beaujeu.bankin

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BankinApplication : Application()