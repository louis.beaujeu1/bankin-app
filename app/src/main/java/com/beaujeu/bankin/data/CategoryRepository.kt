package com.beaujeu.bankin.data

import com.beaujeu.bankin.data.entities.Category
import kotlinx.coroutines.flow.Flow

interface CategoryRepository {
    suspend fun refresh(): List<Category>

    fun observeCategories(parentCategoryId: String?): Flow<List<Category>>
}