package com.beaujeu.bankin.data

import com.beaujeu.bankin.data.entities.Category
import com.beaujeu.bankin.data.local.CategoryDao
import com.beaujeu.bankin.data.local.entities.toDb
import com.beaujeu.bankin.data.local.entities.toDomain
import com.beaujeu.bankin.data.remote.CategoryApiService
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext

class CategoryRepositoryImpl(
    private val categoryDao: CategoryDao,
    private val categoryApiService: CategoryApiService,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : CategoryRepository {
    override suspend fun refresh(): List<Category> = withContext(dispatcher) {
        val categories = categoryApiService.getCategories().categories.map { it.toDb() }
        categoryDao.insertAll(categories)
        categories.map { it.toDomain() }
    }

    override fun observeCategories(parentCategoryId: String?): Flow<List<Category>> {
        return categoryDao.observeCategories(parentCategoryId)
            .map { categories -> categories.map { it.toDomain() } }
    }
}