package com.beaujeu.bankin.data.entities

data class Category(
    val id: String,
    val resourceUri: String,
    val resourceType: String,
    val name: String,
    val parentId: String?,
    val custom: Boolean,
    val other: Boolean,
    val isDeleted: Boolean
) {
    data class Parent(
        val id: String,
        val resourceUri: String,
        val resourceType: String
    )
}