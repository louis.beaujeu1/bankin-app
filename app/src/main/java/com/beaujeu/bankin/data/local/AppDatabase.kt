package com.beaujeu.bankin.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.beaujeu.bankin.data.local.entities.CategoryDb

@Database(entities = [CategoryDb::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun categoryDao(): CategoryDao

    companion object {
        fun build(context: Context, name: String): AppDatabase {
            return Room.databaseBuilder(
                context,
                AppDatabase::class.java,
                name
            ).fallbackToDestructiveMigration().build()
        }
    }
}