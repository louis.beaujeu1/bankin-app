package com.beaujeu.bankin.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.beaujeu.bankin.data.local.entities.CategoryDb
import kotlinx.coroutines.flow.Flow

@Dao
interface CategoryDao {
    @Query("SELECT * FROM category where parentId IS :parentCategoryId")
    fun observeCategories(parentCategoryId: String?): Flow<List<CategoryDb>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(categories: List<CategoryDb>)
}