package com.beaujeu.bankin.data.local.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "category")
data class CategoryDb(
    @PrimaryKey
    val id: String,
    val resourceUri: String,
    val resourceType: String,
    val name: String,
    val parentId: String? = null,
    val custom: Boolean,
    val other: Boolean,
    val isDeleted: Boolean
)