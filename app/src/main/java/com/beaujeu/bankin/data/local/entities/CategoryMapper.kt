package com.beaujeu.bankin.data.local.entities

import com.beaujeu.bankin.data.entities.Category
import com.beaujeu.bankin.data.remote.entities.CategoryDto

fun CategoryDto.toDb(): CategoryDb = CategoryDb(
    id = id,
    resourceUri = resourceUri,
    resourceType = resourceType,
    name = name,
    parentId = parent?.id,
    custom = custom,
    other = other,
    isDeleted = isDeleted
)

fun CategoryDb.toDomain(): Category = Category(
    id = id,
    resourceUri = resourceUri,
    resourceType = resourceType,
    name = name,
    parentId = parentId,
    custom = custom,
    other = other,
    isDeleted = isDeleted
)