package com.beaujeu.bankin.data.remote

import com.beaujeu.bankin.data.remote.entities.CategoriesDto
import retrofit2.http.GET

interface CategoryApiService {
    @GET("/bankin-engineering/challenge-android/master/categories.json")
    suspend fun getCategories(): CategoriesDto
}