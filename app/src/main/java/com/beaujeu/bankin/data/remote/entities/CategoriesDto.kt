package com.beaujeu.bankin.data.remote.entities

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CategoriesDto(
    @Json(name = "resources")
    val categories: List<CategoryDto>
)