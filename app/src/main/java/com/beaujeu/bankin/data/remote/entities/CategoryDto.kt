package com.beaujeu.bankin.data.remote.entities

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CategoryDto(
    val id: String,
    @Json(name = "resource_uri")
    val resourceUri: String,
    @Json(name = "resource_type")
    val resourceType: String,
    val name: String,
    val parent: ParentDto?,
    val custom: Boolean,
    val other: Boolean,
    @Json(name = "is_deleted")
    val isDeleted: Boolean
) {
    @JsonClass(generateAdapter = true)
    data class ParentDto(
        val id: String,
        @Json(name = "resource_uri")
        val resourceUri: String,
        @Json(name = "resource_type")
        val resourceType: String
    )
}