package com.beaujeu.bankin.injection

import android.app.Application
import com.beaujeu.bankin.data.local.AppDatabase
import com.beaujeu.bankin.data.local.CategoryDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {
    @Singleton
    @Provides
    fun providesAppDatabase(application: Application): AppDatabase {
        return AppDatabase.build(context = application.applicationContext, name = DATABASE_NAME)
    }

    @Singleton
    @Provides
    fun provideCategoryDao(appDatabase: AppDatabase): CategoryDao = appDatabase.categoryDao()

    private companion object {
        const val DATABASE_NAME = "bankin_db"
    }
}