package com.beaujeu.bankin.injection

import com.beaujeu.bankin.data.CategoryRepository
import com.beaujeu.bankin.data.CategoryRepositoryImpl
import com.beaujeu.bankin.data.local.CategoryDao
import com.beaujeu.bankin.data.remote.CategoryApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {
    @Provides
    fun provideCategoryRepository(
        categoryApiService: CategoryApiService,
        categoryDao: CategoryDao
    ): CategoryRepository = CategoryRepositoryImpl(categoryDao, categoryApiService)
}