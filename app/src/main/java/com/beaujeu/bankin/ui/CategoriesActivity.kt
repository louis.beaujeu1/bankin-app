package com.beaujeu.bankin.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.DividerItemDecoration
import com.beaujeu.bankin.R
import com.beaujeu.bankin.databinding.CategoriesActivityBinding
import com.beaujeu.bankin.ui.categories.CategoriesAdapter
import com.beaujeu.bankin.ui.categories.CategoriesViewModel
import com.beaujeu.bankin.ui.categories.CategoriesViewModel.Companion.PARENT_CATEGORY_ID_KEY
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class CategoriesActivity : AppCompatActivity() {

    private lateinit var binding: CategoriesActivityBinding

    private val viewModel: CategoriesViewModel by viewModels()
    private lateinit var adapter: CategoriesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = CategoriesActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupToolbar()
        setupRecyclerView()
        setupSwipeRefresh()
        setupRetryButton()

        observeViewModel()

        if (savedInstanceState == null) {
            viewModel.refresh()
        }
    }

    private fun setupRecyclerView() {
        adapter = CategoriesAdapter {
            viewModel.onCategoryClicked(categoryId = it)
        }

        with(binding) {
            categories.addItemDecoration(
                DividerItemDecoration(
                    this@CategoriesActivity,
                    DividerItemDecoration.VERTICAL
                )
            )
            categories.adapter = adapter

            swipeRefresh.setOnRefreshListener {
                viewModel.refresh()
            }
        }
    }

    private fun setupToolbar() {
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.toolbar.setNavigationOnClickListener {
            finish()
        }
    }

    private fun setupSwipeRefresh() {
        binding.swipeRefresh.setOnRefreshListener {
            viewModel.refresh()
        }
    }

    private fun setupRetryButton() {
        binding.retryButton.setOnClickListener {
            viewModel.refresh()
        }
    }

    private fun observeViewModel() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.uiState.collectLatest(::handleUiState)
            }
        }

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.navigationEvent.collect { categoryId ->
                    startActivity(
                        newIntent(
                            context = this@CategoriesActivity,
                            categoryId = categoryId
                        )
                    )
                }
            }
        }
    }

    private fun handleUiState(state: CategoriesViewModel.UiState) {
        binding.swipeRefresh.isRefreshing = state.isLoading
        adapter.submitList(state.categories)

        binding.retryView.isVisible = state.isError && state.categories.isEmpty()

        if (state.isError && state.categories.isNotEmpty()) {
            Snackbar.make(binding.root, R.string.unknown_error, Snackbar.LENGTH_SHORT).show()
            viewModel.onErrorMessageShown()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.categories.adapter = null // clear adapter to avoid a memory leak
    }

    companion object {
        fun newIntent(context: Context, categoryId: String? = null): Intent =
            Intent(context, CategoriesActivity::class.java).apply {
                putExtra(PARENT_CATEGORY_ID_KEY, categoryId)
            }
    }
}