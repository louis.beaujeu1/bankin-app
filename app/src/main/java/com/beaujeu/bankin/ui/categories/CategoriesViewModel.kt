package com.beaujeu.bankin.ui.categories

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.beaujeu.bankin.data.CategoryRepository
import com.beaujeu.bankin.data.entities.Category
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CategoriesViewModel @Inject constructor(
    private val categoriesRepository: CategoryRepository,
    private val savedStateHandle: SavedStateHandle,
) : ViewModel() {

    data class UiState(
        val isLoading: Boolean,
        val isError: Boolean,
        val categories: List<Category>
    )

    private val parentCategoryId: String?
        get() = savedStateHandle[PARENT_CATEGORY_ID_KEY]

    private val _navigationEvent = MutableSharedFlow<String>()
    val navigationEvent: Flow<String> = _navigationEvent.asSharedFlow()

    private val _uiState = MutableStateFlow(
        UiState(
            isLoading = false,
            isError = false,
            categories = emptyList()
        )
    )
    val uiState = _uiState.asStateFlow()

    init {
        viewModelScope.launch {
            categoriesRepository.observeCategories(parentCategoryId).collectLatest { categories ->
                _uiState.update {
                    it.copy(categories = categories)
                }
            }
        }
    }

    fun refresh() {
        viewModelScope.launch {
            try {
                _uiState.update { it.copy(isLoading = true, isError = false) }
                categoriesRepository.refresh()
            } catch (e: Exception) {
                _uiState.update { it.copy(isError = true) }
            } finally {
                _uiState.update { it.copy(isLoading = false) }
            }
        }
    }

    fun onCategoryClicked(categoryId: String) {
        viewModelScope.launch {
            _navigationEvent.emit(categoryId)
        }
    }

    fun onErrorMessageShown() {
        _uiState.update { it.copy(isError = false) }
    }

    companion object {
        const val PARENT_CATEGORY_ID_KEY = "parent_category_id"
    }
}