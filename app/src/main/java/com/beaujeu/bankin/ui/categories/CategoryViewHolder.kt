package com.beaujeu.bankin.ui.categories

import androidx.recyclerview.widget.RecyclerView
import com.beaujeu.bankin.data.entities.Category
import com.beaujeu.bankin.databinding.CategoryItemBinding

class CategoryViewHolder(private val binding: CategoryItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(category: Category, onCategoryClicked: ((String) -> Unit)?) {
        binding.categoryName.text = category.name
        onCategoryClicked?.let {
            binding.root.setOnClickListener {
                it(category.id)
            }
        }
    }
}
